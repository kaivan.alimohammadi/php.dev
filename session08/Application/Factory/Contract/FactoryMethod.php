<?php

namespace Application\Factory\Contract;

use Application\Cars\Contract\VehicleInterface;

abstract class FactoryMethod {
	const CHEAP = 'cheap';
	const FAST = 'fast';

	abstract protected function createVehicle( string $type ): VehicleInterface;

	public function create( string $type, string $color ): VehicleInterface {
		$obj = $this->createVehicle( $type );
		$obj->setColor( $color );

		return $obj;
	}
}