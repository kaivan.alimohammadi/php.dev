<?php
namespace Application\Cars;


use Application\Cars\Contract\VehicleInterface;

class Bicycle implements VehicleInterface {

	public function setColor( string $color ) {
		print "Bicycle With Color :{$color}";
	}
}