<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 6:24 PM
 */

namespace Application\Notification\Factory;
use Application\Notification\Gateways\SMS\Contract\SMSInterface;

class SMSGateFactory {

	public static function getSmsHandler( string $name ): SMSInterface {
//		switch ( $name ) {
//			case 'sib':
//				return new SibSms();
//				break;
//			case 'fara':
//				return new FaraSms();
//				break;
//			default:
//				return false;
//				break;
//		}
		$handler = 'Application\Notification\Gateways\SMS\\'.ucfirst($name).'Sms';
//		var_dump($handler);
		return new $handler;

	}

}