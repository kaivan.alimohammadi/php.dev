<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 6:43 PM
 */

namespace Application\Notification\Gateways\SMS;


use Application\Notification\Gateways\SMS\Contract\SMSInterface;

class IranSms implements SMSInterface {

	public function send( array $to, $message ) {
		$to  = implode(',',$to);
		print "iransms -> {$message} to {$to} ";
	}
}