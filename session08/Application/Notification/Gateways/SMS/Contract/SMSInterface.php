<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 6:22 PM
 */

namespace Application\Notification\Gateways\SMS\Contract;


interface SMSInterface {

	public function send(array $to,$message);

}