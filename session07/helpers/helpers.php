<?php
if(!function_exists('now')){
	function now($timeZone='Asia/Tehran'){
		return \Carbon\Carbon::now($timeZone);
	}
}

if(!function_exists('product'))
{
	function product(){
		return new \App\Models\Product\Product();
	}
}