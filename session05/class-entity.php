<?php

abstract class Entity {

	protected $table;

	protected $primaryKey;

	protected $connection;

	protected $stmt;

	public function __construct() {
		try {
			$this->connection = new PDO( "mysql:host=localhost;dbname=php.dev;charset=utf8", "root", "" );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage();
		}
	}

	public function find( $id, $columns = null ) {

		$columns = is_null( $columns ) ? '*' : implode( ',', $columns );

		$this->stmt = $this->connection->prepare( "SELECT {$columns} FROM {$this->table} WHERE {$this->primaryKey}={$id} LIMIT 1" );
		$this->execute();

		return $this->stmt->fetch( PDO::FETCH_ASSOC );

	}

	public function findBy( $criteria, $columns, $single = false ) {
		$query     = "";
		$columns   = is_null( $columns ) ? '*' : implode( ',', $columns );
		$sql_query = "SELECT {$columns} FROM {$this->table} WHERE ";
		foreach ( $criteria as $key => $value ) {
			switch ( $value ) {
				case is_int( $value ):
					$sql_query .= "{$key}={$value} AND ";
					break;
				case is_string( $value ):
					$sql_query .= "{$key}='{$value}' AND ";
					break;
			}
		}
		$sql_query=rtrim($sql_query,'AND ');
		if ( $single ) {
			$sql_query .= " LIMIT 1";
		}
		$this->stmt = $this->connection->prepare( $sql_query );
		$this->stmt->execute();

		return $this->stmt;

	}

	public function create( $data ) {
		$keys         = array_keys( $data );
		$values       = array_values( $data );
		$columns      = implode( ',', $keys );
		$query_values = "";
		foreach ( $values as $value ) {
			$query_values .= "'" . $value . "',";
		}
		$query_values = rtrim( $query_values, ',' );

		$this->stmt = $this->connection->prepare( "INSERT INTO {$this->table}({$columns})VALUES({$query_values})" );
		$this->execute();
	}

	public function delete( $id ) {
		$this->stmt = $this->connection->prepare( "DELETE FROM {$this->table} WHERE {$this->primaryKey} = {$id} LIMIT 1" );
		$this->execute();
	}

	public function update( $id, $data ) {
		$sets = "";
		foreach ( $data as $key => $value ) {
			$sets .= "{$key}={$value},";
		}
		$sets       = rtrim( $sets, ',' );
		$this->stmt = $this->connection->prepare( "UPDATE {$this->table}
		SET {$sets} WHERE {$this->primaryKey}={$id}" );
		$this->execute();
	}

	protected function execute() {
		if ( ! is_null( $this->stmt ) ) {
			$this->stmt->execute();
		}
	}

	abstract protected function save();

}
