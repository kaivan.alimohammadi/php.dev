<?php
include "class-entity.php";
class Product extends Entity
{

	public function __construct() {
		parent::__construct();
		$this->table = 'products';
		$this->primaryKey = 'product_id';
	}

	public function getAllActiveProducts() {
		$this->stmt = $this->connection->prepare("SELECT * FROM {$this->table} WHERE active=1");
		$this->execute();
		return $this->stmt->fetchAll();
	}
}

$p = new Product();
$sample_product = $p->find(1);
print_r($sample_product);