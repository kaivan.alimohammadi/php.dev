<?php

include "products.php";
$products = getProducts();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment</title>
</head>
<body style="direction: rtl;padding: 50px">
	<form action="payment.php" method="post">
		<table>
			<tr>
				<th>محصول</th>
				<th>قیمت</th>
				<th>تعداد</th>
				<th>قیمت کل</th>
			</tr>
			<?php foreach ($products as $p): ?>
				<tr>
					<td><?php echo $p['title']; ?></td>
					<td><?php echo $p['price']; ?></td>
					<td><?php echo $p['count']; ?></td>
					<td><?php echo $p['price'] * $p['count'];; ?></td>
				</tr>
			<?php endforeach; ?>
		</table>

		<input type="radio" name="paymentMethod" value="account" checked>
		حساب کاربری

		<input type="radio" name="paymentMethod" value="online">
		درگاه آنلاین

		<input type="radio" name="paymentMethod" value="cod">
		درب منزل
		<br>
        <input type="hidden" name="totalPrice" value="3200000">
		<button type="submit" name="pay">پرداخت</button>
	</form>
</body>
</html>