<?php

abstract class Gateway
{
	public function __construct() {

	}
	abstract protected function paymentRequest();

	abstract protected function paymentVerify();

}