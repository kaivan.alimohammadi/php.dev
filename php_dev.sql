-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 04:40 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php.dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_persian_ci NOT NULL,
  `content` text COLLATE utf8_persian_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `code` varchar(15) COLLATE utf8_persian_ci NOT NULL,
  `reply` text COLLATE utf8_persian_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `user_id`, `full_name`, `mobile`, `content`, `created_at`, `code`, `reply`) VALUES
(1, NULL, 'پدرام بهرامی ', '09123456789', 'متن درخواست تست', '2017-10-30 14:55:19', '6CB090BD01', 'پاسخ شما ارسال شد.'),
(2, NULL, 'علی', '09112345678', 'درخواست تست', '2017-10-30 15:31:53', '8AAC33370D', 'تتس'),
(3, 2, '', 'تست', 'تست', '2017-10-30 15:33:11', 'B23C36A36D', NULL),
(4, NULL, 'تست', '09123456987', 'ملذندیسهخحلاشسیخحل', '2017-11-01 14:39:27', '2ED2FB73B9', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_title` varchar(250) COLLATE utf8_persian_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_price`, `product_created_at`) VALUES
(1, 'گوشی موبایل سامسونگ', 2300000, '2017-11-01 08:23:10'),
(2, 'لپ تاپ سونی', 4300000, '2017-10-26 15:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_persian_ci DEFAULT NULL,
  `user_email` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `user_password` varchar(150) COLLATE utf8_persian_ci NOT NULL,
  `user_registered_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `user_email`, `user_password`, `user_registered_at`) VALUES
(2, 'پدرام', 'فهیم', 'pedram@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '2017-10-25 16:56:38'),
(3, 'پدرام', 'فهیم', 'pedram@gmail.com', '6f1dd1edb0cead9ddb3b9147d8358c89', '2017-10-25 16:58:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
